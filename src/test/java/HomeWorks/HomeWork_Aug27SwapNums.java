package HomeWorks;

import java.util.Scanner;

public class HomeWork_Aug27SwapNums {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a,b;
		System.out.println("Enter any two numbers ");
		Scanner scan=new Scanner(System.in);
		a=scan.nextInt();
		b=scan.nextInt();
		System.out.println("Before Swapping first number is "+a+" Second Number is "+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After Swapping first number is "+a+" Second Number is "+b);
		
	}

}
