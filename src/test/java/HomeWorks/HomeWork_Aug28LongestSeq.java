package HomeWorks;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class HomeWork_Aug28LongestSeq {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		
		System.out.println("Enter number of inputs ");
		Scanner scan=new Scanner(System.in);
		n=scan.nextInt();
		int a[] = new int[n];
		
		System.out.println("Enter inputs ");
		for(int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		//longestConse(a);
		System.out.println("Output ");
		int output;
		
		for(int i=0;i<n;i++)
		{
			output=longestConse(a);
			System.out.println(output);
			
		}
		
	}
	public static int longestConse(int[] a)
	{
		int max=1;
		Set<Integer> set=new HashSet<Integer>();
		
		for(int e:a)
		{
			set.add(e);
			
		}
		for(int e:a)
		{
			int left = e-1;
			int right = e+1;
			int count=1;
			while(set.contains(left))
			{
				count++;
				set.remove(left);
				left--;
			}
			while(set.contains(right))
			{
				count++;
				set.remove(right);
				right++;
				
			}
			max=Math.max(count, max);
		}
		return max;	
		
		
		
		
		
	}

}
