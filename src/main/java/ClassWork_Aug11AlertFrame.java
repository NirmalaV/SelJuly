import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ClassWork_Aug11AlertFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		//String text = driver.switchTo().alert().getText();
		//System.out.println(text);
		driver.switchTo().alert().sendKeys("Nirmala Nirai");
		
		driver.switchTo().alert().accept();
		String str = driver.findElementById("demo").getText();
		//String str = driver.findElementByXPath("//p[text()='Hello Nirmala Nirai! How are you today?']").getText();
		System.out.println(str);
		
		
	
	}

}
