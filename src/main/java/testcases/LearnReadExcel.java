package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnReadExcel {

	public static Object[][] getExcelData(String CreateLead) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook=new XSSFWorkbook("./data/CreateLead.xlsx");
	 XSSFSheet sheet = wbook.getSheetAt(0);
	 int lastRownum=sheet.getLastRowNum();
	 int lastCellNum = sheet.getRow(0).getLastCellNum();
	 Object[][] data=new Object[lastRownum][lastCellNum];
	 for (int i =1 ; i <=lastRownum; i++) {
		 XSSFRow row = sheet.getRow(i);
		 for (int j = 0; j < lastCellNum; j++) {
			 XSSFCell cell = row.getCell(j);
			 String stringCellValue = cell.getStringCellValue();
			 
			data[i-1][j]=stringCellValue;
		 System.out.println(stringCellValue);
		 
		}
	}
	return data;
		
	}

}
