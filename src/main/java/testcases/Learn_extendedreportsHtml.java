package testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Learn_extendedreportsHtml {
		
		 public static ExtentTest test;
		 public static ExtentReports extent;
		 public static String author,category;
		 public static String testcasename ="TC002_CreateLeads";
		 public static String testcasedesc="Create a new lead";
		 
		// TODO Auto-generated method stub
		@BeforeSuite
		public void startResult()
		{
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/result1.html");
		html.setAppendExisting(true);
		
		extent=new ExtentReports();
		extent.attachReporter(html);
		
		}
		@BeforeMethod
		public void startTestCase()
		{
		test = extent.createTest(testcasename, testcasedesc);

		test.assignAuthor(author);
		test.assignCategory(category);
		}
		
		public void reportStep(String desc,String status)
		{
			if(status.equalsIgnoreCase("pass"))
			test.pass(desc);
			if(status.equalsIgnoreCase("fail"))
				test.fail(desc);
		}
		
		//test.pass("The data DemoSalesManager has been entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		//test.pass("The data crmsfa has been entered successfully");
		//test.pass("Login button clicked successfully");
		
		
		@AfterSuite
		public void stopResult()
		{
		extent.flush();
		}
		

	}

