package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TestCase_CreateLead extends TC001_Login{
	@Test
	public void createLead()
	{
		 login();
		 WebElement crmsfa=locateElement("LinkText","CRM/SFA");
		 click(crmsfa);
		 
		 WebElement createlead=locateElement ("LinkText","Create Lead");
		 click(createlead);
		 
		 WebElement compname=locateElement("id","createLeadForm_companyName");
		 type(compname,"Wipro");
		 
		 WebElement firstName=locateElement("id","createLeadForm_firstName");
		 type(firstName,"Nirmala");
		 
		 WebElement lastName = locateElement("id","createLeadForm_lastName");
		 type(lastName,"V");
		 
		 WebElement sourcedd=locateElement("id","createLeadForm_dataSourceId");
		 selectDropDownUsingText(sourcedd,"Employee");
		 
		 WebElement marketcampdd=locateElement("id","createLeadForm_marketingCampaignId");
		 selectDropDownUsingText(marketcampdd,"Car and Driver");
		 
		 WebElement phonenum=locateElement("id","createLeadForm_primaryPhoneNumber");
		 type(phonenum,"9791054026");
		 
		 WebElement email=locateElement("id","createLeadForm_primaryEmail");
		 type(email,"nirmala.victor2015@gmail.com");
		 
		 WebElement createSubmit=locateElement("class","smallSubmit");
		 click(createSubmit);
		 
		 
	}
}
