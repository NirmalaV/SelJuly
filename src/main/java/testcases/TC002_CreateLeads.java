package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_CreateLeads extends ProjectMethods{
	@Test(groups="smoke",dataProvider="ExcelData")
	
	public void createLeads(String cname,String fname,String lname,String email,String phNum){
		//TC001_Login obj=new TC001_Login();
		//obj.login();
		/*login();
		WebElement cl = locateElement("LinkText","CRM/SFA");
		click(cl);*/
		
		WebElement createlead=locateElement("LinkText","Create Lead");
		click(createlead);
		
		WebElement compname=locateElement("id","createLeadForm_companyName");
		type(compname,cname);
		
		WebElement firstname=locateElement("id","createLeadForm_firstName");
		type(firstname,fname);
		
		WebElement lastname=locateElement("xpath","(//input[@id='createLeadForm_lastName'])[1]");
		type(lastname,lname);
		
		WebElement dropdown=locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(dropdown,"Conference");
		
		WebElement title=locateElement("id","createLeadForm_generalProfTitle");
		type(title,"System Analyst");
		
		WebElement submit=locateElement("class","smallSubmit");
		click(submit);	
		
	}

	@DataProvider(name="createLeadsDataProvider")
	public Object[][] createLeadsDataProvider(){
		
		Object[][] data=new Object[3][3];
		
		data[0][0]="Alight";
		data[0][1]="Nirmala";
		data[0][2]="V";
		
		
		data[1][0]="Wipro";
		data[1][1]="Nirmala";
		data[1][2]="V";
		
		data[2][0]="CapGemini";
		data[2][1]="Nirmala";
		data[2][2]="V";
		
		
		return data;
		
		
		
		
		
		
				
	}
		
	
	
}
