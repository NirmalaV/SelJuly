package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TestCase_DuplicateLead extends TC001_Login {
	@Test
	public void duplicatelead()
	{
		login();
		
		WebElement cl = locateElement("LinkText","CRM/SFA");
		click(cl);
		
		WebElement lead=locateElement("LinkText","Leads");
		click(lead);
		
		WebElement findlead=locateElement("LinkText","Find Leads");
		click(findlead);
		
		WebElement emailaddr=locateElement("xpath","//input[@name='emailAddress']");
		type(emailaddr,"nirmala.victor2015@gmail.com");
		
		WebElement clickfindleads=locateElement("xpath","//button[text()='Find Leads']");
		click(clickfindleads);
		
		WebElement nameCaptured=locateElement("xpath","//a[text()='Nirmala']");
		click(nameCaptured);
		
		WebElement duplbutton=locateElement("LinkText","Duplicate Lead");
		click(duplbutton);
		
		WebElement createLead=locateElement("class","smallSubmit");
		click(createLead);
		
		closeBrowser();
		
		
		
	}
}
