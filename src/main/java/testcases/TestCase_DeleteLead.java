package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestCase_DeleteLead extends ProjectMethods {
	//@Test(dependsOnMethods="testcases.TC003_EditLead.editMethod")\
	@Test(groups="Regression" , dependsOnGroups="sanity")
	public void deletelead()
	{
		/*login();
		WebElement cl = locateElement("LinkText","CRM/SFA");
		click(cl);*/
		
		WebElement lead=locateElement("LinkText","Leads");
		click(lead);
		
		WebElement findlead=locateElement("LinkText","Find Leads");
		click(findlead);
		
		WebElement phtab=locateElement("xpath","//span[text()='Phone']");
		click(phtab);
		
		WebElement phnum=locateElement("xpath","//input[@name='phoneNumber']");
		type(phnum,"9791054026");
		
		WebElement fl=locateElement("xpath","//button[text()='Find Leads']");
		click(fl);
		
		WebElement firstresult=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(firstresult);
		
		WebElement deleteresult=locateElement("class","subMenuButtonDangerous");
		click(deleteresult);
		
		WebElement findlead2=locateElement("LinkText","Find Leads");
		click(findlead2);
		
		WebElement idcaptured=locateElement("xpath","(//input[@autocomplete='off'])[30]");
		type(idcaptured,"10292");
		
		WebElement findlead3=locateElement("xpath","//button[text()='Find Leads']");
		click(findlead3);
		
		closeBrowser();
		
		
		
		
	}

}
