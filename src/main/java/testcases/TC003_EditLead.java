package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.cucumber.datatable.dependency.difflib.Delta.TYPE;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods
{
	//@Test(groups="sanity" , dependsOnGroups="smoke")
	@Test(groups="sanity",dataProvider="editleaddataprovider")
	public void findlead(String fname,String compname) throws InterruptedException
	{
		/*login();
		WebElement cl = locateElement("LinkText","CRM/SFA");
		click(cl);*/
		
		WebElement lead=locateElement("LinkText","Leads");
		click(lead);
		
		WebElement findlead=locateElement("LinkText","Find Leads");
		click(findlead);
		
		WebElement firstname=locateElement("xpath","(//input[@name='firstName'])[3]");
		type(firstname,fname);
		Thread.sleep(20);
		WebElement findbutton=locateElement("xpath","(//button[@type='button'])[7]");
		click(findbutton);
		
		WebElement firstlead=locateElement("xpath","(//a[@class='linktext'])[4]");
		click(firstlead);
		
		WebElement edtlead=locateElement("xpath","//a[text()='Edit']");
		click(edtlead);
		
		WebElement updateComp=locateElement("id","updateLeadForm_companyName");
		//TYPE delete = TYPE.DELETE;
		type(updateComp,compname);
		
		
		WebElement updatebutton=locateElement("xpath","(//input[@name='submitButton'])[1]");
		click(updatebutton);
		
		closeBrowser();
		
		System.out.println("Company Name updated to ");
		
	}
	@DataProvider(name="editleaddataprovider")
	public Object[][] editleaddataprovider()
	{
		Object[][] data=new Object[2][1];
		
		data[0][0]="Nirmala";
		data[0][1]="CTS";
		
		data[1][0]="Nirmala";
		data[1][1]="HoneyWell";
		
		data[2][0]="Nirmala";
		data[2][1]="Hexaware";
		
		return data;
		
	}

}
