package tests;

import org.testng.annotations.Test;

import pages.Homepage;
import wdMethods.PreAndPost;

public class BankBazar_Tc001 extends PreAndPost{
	@Test
	public void bankbazar_testcase()
	{
		new Homepage()
		.clickMutualfunds()
		.clickSearchmFunds()
		.clickMyAge()
		.clickMonth()
		.clickBday()
		.clickOnContinue()
		.selectIncome()
		.clickOnContinue()
		.selectBank()
		.firstname()
		.viewMutualfunds()
		.printallSchemes();
		
	}

}
