package tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZoomCar {
	@Test
	public void zoomCar_project() throws IOException, InterruptedException
	{
		// launch the browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");

		driver.manage().window().maximize();

		Thread.sleep(1000);
		//click on 'Start your wonderful journey
		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		Thread.sleep(1000);
		//click on Kodambakkam
		driver.findElementByXPath("//div[contains(text(),'Kodambakkam')]").click();
		//click on next button
		driver.findElementByXPath("//button[text()='Next']").click();
		//click on start date as tomorrow
		driver.findElementByXPath("//div[text()='Mon']").click();
		//click on next button
		driver.findElementByXPath("//button[text()='Next']").click();
		System.out.println();

		//click on done button
		driver.findElementByXPath("//button[text()='Done']").click();

		Thread.sleep(3000);
		List<WebElement> list = driver.findElementsByClassName("car-listing");
		System.out.println("Number of results displayed "+list.size());

		Thread.sleep(3000);

		List<WebElement> price = driver.findElementsByXPath("//div[@class='price']");
		System.out.println("Price "+price.size());

		System.out.println("Maximum price ");

		List<String> allcars=new ArrayList<String>();

		for(WebElement allcarsprice:price)
		{
			allcars.add(allcarsprice.getText().replaceAll("\\D", " "));

		}
		String max=Collections.max(allcars);

		System.out.println(max);

		System.out.println("Maximum price with brand name ");

		List<WebElement> list2 = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement brandname : list2) 
		{
		allcars.add(brandname.getText().replaceAll("\\D", ""));
		}
		
		String brandmax=Collections.max(allcars);
		System.out.println(brandmax);
	}
}



