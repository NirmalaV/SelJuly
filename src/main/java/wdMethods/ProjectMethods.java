package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import testcases.LearnReadExcel;

public class ProjectMethods extends SeMethods {
	public static String datasheetname;
	
	@Parameters({"browser","appUrl", "userName","password"})
	
	@BeforeMethod (groups="Common")
	
	public void login_T(String browser,String appUrl,String userName,String password)
	{
		
			startApp(browser, appUrl);
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, userName);
			WebElement elePassword = locateElement("id","password");
			type(elePassword, password);
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);
			
			WebElement cl = locateElement("LinkText","CRM/SFA");
			click(cl);
			
			
		
	}
	@AfterMethod(groups="Common")
	public void close()
	{
		closeBrowser();
		
	}
	@DataProvider(name="ExcelData")
	public  Object[][] fetchData() throws IOException {
		
	Object[][] excelData = LearnReadExcel.getExcelData(datasheetname);
	
		return excelData;
		
		
		
		
	}

}
