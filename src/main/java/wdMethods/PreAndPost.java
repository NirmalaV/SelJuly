package wdMethods;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class PreAndPost extends SeMethods {
	
	@BeforeMethod
	public void before() {
		startApp("chrome", "https://www.bankbazaar.com");
	}
	
	@AfterMethod
	public void after() {
		closeBrowser();
	}

}
