
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testcases.TC001_Login;

public class MergeLead {

	
	public static void main(String[] args) throws InterruptedException, IOException {
		
        //Open browser
	
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		//create obj
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//Open URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Login
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemosalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		Thread.sleep(1000);
		// Click crm/sfa link
		driver.findElementByXPath("//div[@for='crmsfa']/a").click();
		Thread.sleep(1000);
		//Click Leads link
		driver.findElementByLinkText("Leads").click();
		Thread.sleep(1000);
		//Click Merge leads
		driver.findElementByLinkText("Merge Leads").click();
		Thread.sleep(1000);
		//Click on Icon near From Lead
		driver.findElementByXPath("//img[@src='/images/fieldlookup.gif']").click();
		//Move to new window
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allwindows=new ArrayList<String>();
		allwindows.addAll(windowHandles);
		String secondWindow=allwindows.get(1);
		String firstWindow=allwindows.get(0);
		driver.switchTo().window(secondWindow);
		Thread.sleep(2000);
		//Enter Lead ID
		driver.findElementByXPath("//input[@name='id']").sendKeys("10052");
		//Click Find Leads
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//Click First Resulting lead
		WebDriverWait wait=new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"))).click();
		//Switch back to primary window
		driver.switchTo().window(firstWindow);		
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		//Move to new window
		Set <String> windowHandles1=driver.getWindowHandles();
		List<String> allwindowhandles1=new ArrayList<String>();
		allwindowhandles1.addAll(windowHandles1);
		String firstWindow1=allwindowhandles1.get(0);
		String secondWindow1=allwindowhandles1.get(1);
		driver.switchTo().window(secondWindow1);
		//Enter Lead ID
		driver.findElementByXPath("//input[@name='id']").sendKeys("10060");
		//Click Find Leads
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//Click First Resulting lead
		WebDriverWait wait1=new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"))).click();
		//Switch back to primary window
		driver.switchTo().window(firstWindow1);	
		//Click Merge
		driver.findElementByLinkText("Merge").click();
		//Accept Alert
		WebDriverWait wait3=new WebDriverWait(driver,10);
		wait3.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
		Thread.sleep(2000);
		//Click Find Leads
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//input[@name='id']").sendKeys("10052");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		//Verify error msg and Take a screenshot of the page and save in a folder
		WebElement message=driver.findElementByXPath("//div[@class='x-paging-info']");
		String text1=message.getText();
		if(text1.equals("No records to display"))
		{
			System.out.println("Merge successful");
			File source =driver.getScreenshotAs(OutputType.FILE);
			File dest=new File("./snaps/img.png");
			FileUtils.copyFile(source, dest);
			
		}
		
		else
		{
			System.out.println("Merge Unsuccessfull");
		}
		//Close the browser (Do not log out)
		driver.quit();
	}
}