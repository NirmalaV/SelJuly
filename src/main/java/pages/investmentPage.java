package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class investmentPage extends PreAndPost {
	public investmentPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public investmentPage printallSchemes()
	{
		List<WebElement> allSchemes = driver.findElementsByClassName("js-offer-name");
		for (WebElement eachScheme : allSchemes) 
		{
			System.out.println(eachScheme.getText());
			WebElement eleAmount = driver.findElementByXPath("//span[contains(text(),'"+eachScheme.getText()+"')]/following::span[@class='fui-rupee bb-rupee-xs']/..");
			System.out.println(eleAmount.getText());
		}
	
		return this;
	}

}
