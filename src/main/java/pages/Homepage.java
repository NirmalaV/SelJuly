package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class Homepage extends PreAndPost {

	public Homepage() {
		PageFactory.initElements(driver,this);
	}
	@FindBy
	WebElement elemutual;
	
	public MutualFundsPage clickMutualfunds() {
		elemutual = locateElement("xpath", "//a[text()='INVESTMENTS']");
		WebElement eleinvest = locateElement("xpath", "//a[text()='Mutual Funds']");
		Actions builder = new Actions(driver);
		builder.moveToElement(elemutual).pause(3000).perform();
		click(eleinvest);
		return new MutualFundsPage();
	}
	
}
