package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class MutualFundsPage extends PreAndPost {

	public MutualFundsPage()
	
	{
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(linkText="Search for Mutual Funds")
	WebElement eleSearch;
	
	public selectAgePage clickSearchmFunds() {
		//WebElement eleCreateLead = locateElement("linkText", "Create Lead");
		click(eleSearch);
		return new selectAgePage();
	}
	
}
