package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class personalDetailsPage extends PreAndPost{
	public personalDetailsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	@FindBy
	WebElement fname;
	public personalDetailsPage firstname()
	{
		fname=locateElement("Name","firstName");
		type(fname,"Nirmala");
		return this;
		
	}
	@CacheLookup
	@FindBy
	WebElement clickViewMF;
	public investmentPage viewMutualfunds()
	{
		clickViewMF=locateElement("xpath","//a[text()='View Mutual Funds']");
		click(clickViewMF);
		return new investmentPage();
	}
}
