package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.PreAndPost;

public class selectBankPage extends PreAndPost {
	String bank = "HDFC";

	public selectBankPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	public personalDetailsPage selectBank()
	{
		WebElement elebank=locateElement("xpath","//span[text()='"+bank+"']");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(elebank));
		click(elebank);
		return new personalDetailsPage();
	}
}
