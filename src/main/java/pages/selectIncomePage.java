package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class selectIncomePage extends PreAndPost {
	
	public selectIncomePage()
	{
		PageFactory.initElements(driver, this);
	}
	String salary="500000";
	
	@CacheLookup
	
	WebElement eleIncome;
	
	public selectIncomePage selectIncome()
	{
		eleIncome=locateElement("Name","netAnnualIncome");
		type(eleIncome,salary);
		return this;
	}
	
	@CacheLookup
	@FindBy(xpath="//a[text()='Continue']")
	WebElement clickContinue;
	
	public selectBankPage clickOnContinue()
	{
		click(clickContinue);
		return new selectBankPage();
	}
	

}
