package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.PreAndPost;

public class selectAgePage extends PreAndPost{
	int age = Integer.parseInt("29");
	String month = "May";
	String date = "16";
	public selectAgePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	//@FindBy(xpath="//div[text()='29']")
	@FindBy
	WebElement selectAge;
	
	public selectAgePage clickMyAge() {
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement slider = driver.findElementByXPath("//div[@class='rangeslider__handle']");
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(slider,(age-18)*8, 0).perform(); // 8 pixels for every age
		
		return this;
	}
	
	@CacheLookup

	WebElement selectMonth;
	public selectAgePage clickMonth()
	{
		selectMonth = locateElement("xpath","//a[contains(text(),'\"+month+\"')]");
		click(selectMonth);
		return this;
		
	}
	
	@CacheLookup
	
	WebElement selectBday;
	
	public selectAgePage clickBday()
	{
		selectBday=locateElement("xpath","//div[contains(text(),'"+date+"')]");
		click(selectBday);
		return this;
	}
	@CacheLookup
	@FindBy(xpath="//span[text()='16 May 1989']")
	WebElement verifyBday;
	public selectAgePage verifyAge()
	{
		verifyExactText(verifyBday, "16 May 1989");
		return this;
		
	}
	@CacheLookup
	@FindBy(xpath="//a[text()='Continue']")
	WebElement clickContinue;
	
	public selectIncomePage clickOnContinue()
	{
		click(clickContinue);
		return new selectIncomePage();
	}
	
	
	
}
